<?php
namespace App\Entity;

use App\Repository\StageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: StageRepository::class)]
class Stage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id;

    #[ORM\ManyToOne(inversedBy: 'stages')]
    private ?Etudiant $etudiant;

    #[ORM\ManyToOne(inversedBy: 'stages')]
    private ?Entreprise $entreprise;

    #[ORM\ManyToOne(inversedBy: 'stages')]
    private ?Tuteur $tuteur;

    #[ORM\ManyToOne(inversedBy: 'stages')]
    private ?Responsable $responsable;

    #[ORM\Column(length: 255)]
    private ?string $mois;

    #[ORM\Column(length: 4)]
    private ?string $annee;

    public function __construct(?Etudiant $etudiant = null, ?Entreprise $entreprise = null, ?Tuteur $tuteur = null, ?Responsable $responsable = null, ?string $mois = null, ?string $annee = null)
    {
        $this->etudiant = $etudiant;
        $this->entreprise = $entreprise;
        $this->tuteur = $tuteur;
        $this->responsable = $responsable;
        $this->mois = $mois;
        $this->annee = $annee;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtudiant(): ?Etudiant
    {
        return $this->etudiant;
    }

    public function setEtudiant(?Etudiant $etudiant): self
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function getTuteur(): ?Tuteur
    {
        return $this->tuteur;
    }

    public function setTuteur(?Tuteur $tuteur): self
    {
        $this->tuteur = $tuteur;

        return $this;
    }

    public function getResponsable(): ?Responsable
    {
        return $this->responsable;
    }

    public function setResponsable(?Responsable $responsable): self
    {
        $this->responsable = $responsable;

        return $this;
    }

    public function getMois(): ?string
    {
        return $this->mois;
    }

    public function setMois(?string $mois): self
    {
        $this->mois = $mois;

        return $this;
    }

    public function getAnnee(): ?string
    {
        return $this->annee;
    }

    public function setAnnee(?string $annee): self
    {
        $this->annee = $annee;

        return $this;
    }
}
