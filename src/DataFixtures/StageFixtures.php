<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Etudiant;
use App\Entity\Responsable;
use App\Entity\Stage;
use App\Entity\Tuteur;
use App\Entity\Entreprise;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class StageFixtures extends Fixture
{
     private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }
    public function load(ObjectManager $manager)
    {
        // Données des étudiants


        $users = [
            [
                'email' => 'john@example.com',
                'password' => 'password1',
                'roles' => ['ROLE_USER'],
            ],
            [
                'email' => 'jane@example.com',
                'password' => 'password2',
                'roles' => ['ROLE_ADMIN'],
            ],
        ];


        $etudiantsData = [
        [
            'nom' => 'Dupont',
            'prenom' => 'Emma',
            'classe' => 'L1 Informatique',
        ],
        [
            'nom' => 'Martin',
            'prenom' => 'Louis',
            'classe' => 'L2 Sciences Économiques',
        ],
        [
            'nom' => 'Dubois',
            'prenom' => 'Camille',
            'classe' => 'L3 Droit',
        ],
        [
            'nom' => 'Bernard',
            'prenom' => 'Julie',
            'classe' => 'M1 Psychologie',
        ],
        [
            'nom' => 'Thomas',
            'prenom' => 'Nicolas',
            'classe' => 'M2 Génie Civil',
        ],
        [
            'nom' => 'Lefevre',
            'prenom' => 'Manon',
            'classe' => 'L1 Biologie',
        ],
        [
            'nom' => 'Petit',
            'prenom' => 'Alexandre',
            'classe' => 'L2 Histoire',
        ],
        [
            'nom' => 'Robert',
            'prenom' => 'Sophie',
            'classe' => 'L3 Langues Étrangères Appliquées',
        ],
        [
            'nom' => 'Girard',
            'prenom' => 'Hugo',
            'classe' => 'M1 Gestion',
        ],
        [
            'nom' => 'Moreau',
            'prenom' => 'Élise',
            'classe' => 'M2 Marketing',
        ],
        [
            'nom' => 'Rousseau',
            'prenom' => 'Mathieu',
            'classe' => 'L1 Sciences Politiques',
        ],
        [
            'nom' => 'Mercier',
            'prenom' => 'Claire',
            'classe' => 'L2 Chimie',
        ],
        [
            'nom' => 'Simon',
            'prenom' => 'Thomas',
            'classe' => 'L3 Physique',
        ],
        [
            'nom' => 'Leroy',
            'prenom' => 'Laura',
            'classe' => 'M1 Journalisme',
        ],
        [
            'nom' => 'Garcia',
            'prenom' => 'Pierre',
            'classe' => 'M2 Architecture',
        ],
    ];


        // Donnée des tuteurs
        $tuteursData = [
    [
        'nom' => 'Martin',
        'prenom' => 'Louis',
        'email' => 'louis.martin@gmail.com',
        'telephone' => '+33123456789',
    ],
    [
        'nom' => 'Dubois',
        'prenom' => 'Camille',
        'email' => 'camille.dubois@hotmail.com',
        'telephone' => '+33234567890',
    ],
    [
        'nom' => 'Bernard',
        'prenom' => 'Julie',
        'email' => 'julie.bernard@yahoo.com',
        'telephone' => '+33345678901',
    ],
    [
        'nom' => 'Thomas',
        'prenom' => 'Nicolas',
        'email' => 'nicolas.thomas@outlook.com',
        'telephone' => '+33456789012',
    ],
    [
        'nom' => 'Lefevre',
        'prenom' => 'Manon',
        'email' => 'manon.lefevre@gmail.com',
        'telephone' => '+33567890123',
    ],
    [
        'nom' => 'Petit',
        'prenom' => 'Alexandre',
        'email' => 'alexandre.petit@yahoo.com',
        'telephone' => '+33678901234',
    ],
    [
        'nom' => 'Robert',
        'prenom' => 'Sophie',
        'email' => 'sophie.robert@hotmail.com',
        'telephone' => '+33789012345',
    ],
    [
        'nom' => 'Girard',
        'prenom' => 'Hugo',
        'email' => 'hugo.girard@gmail.com',
        'telephone' => '+33890123456',
    ],
    [
        'nom' => 'Moreau',
        'prenom' => 'Élise',
        'email' => 'elise.moreau@outlook.com',
        'telephone' => '+33901234567',
    ],
    [
        'nom' => 'Rousseau',
        'prenom' => 'Mathieu',
        'email' => 'mathieu.rousseau@yahoo.com',
        'telephone' => '+36109876543',
    ],
    [
        'nom' => 'Mercier',
        'prenom' => 'Claire',
        'email' => 'claire.mercier@gmail.com',
        'telephone' => '+33210987654',
    ],
    [
        'nom' => 'Simon',
        'prenom' => 'Thomas',
        'email' => 'thomas.simon@hotmail.com',
        'telephone' => '+34321098765',
    ],
    [
        'nom' => 'Leroy',
        'prenom' => 'Laura',
        'email' => 'laura.leroy@yahoo.com',
        'telephone' => '+33432109876',
    ],
    [
        'nom' => 'Garcia',
        'prenom' => 'Pierre',
        'email' => 'pierre.garcia@gmail.com',
        'telephone' => '+34543210987',
    ],
];


        // Données des responsables
       $responsablesData = [

        [
            'nom' => 'Martin',
            'prenom' => 'Sophie',
            'email' => 'sophie.martin@gmail.com',
        ],
        [
            'nom' => 'Dubois',
            'prenom' => 'Thomas',
            'email' => 'thomas.dubois@hotmail.com',
        ],
        [
            'nom' => 'Bernard',
            'prenom' => 'Marie',
            'email' => 'marie.bernard@yahoo.com',
        ],
        [
            'nom' => 'Thomas',
            'prenom' => 'Julie',
            'email' => 'julie.thomas@outlook.com',
        ],
        [
            'nom' => 'Lefevre',
            'prenom' => 'Pierre',
            'email' => 'pierre.lefevre@gmail.com',
        ],
        [
            'nom' => 'Petit',
            'prenom' => 'Isabelle',
            'email' => 'isabelle.petit@yahoo.com',
        ],
        [
            'nom' => 'Robert',
            'prenom' => 'David',
            'email' => 'david.robert@hotmail.com',
        ],
        [
            'nom' => 'Girard',
            'prenom' => 'Camille',
            'email' => 'camille.girard@gmail.com',
        ],
        [
            'nom' => 'Moreau',
            'prenom' => 'Thomas',
            'email' => 'thomas.moreau@outlook.com',
        ],
        [
            'nom' => 'Rousseau',
            'prenom' => 'Elodie',
            'email' => 'elodie.rousseau@yahoo.com',
        ],
        [
            'nom' => 'Mercier',
            'prenom' => 'Marc',
            'email' => 'marc.mercier@gmail.com',
        ],
        [
            'nom' => 'Simon',
            'prenom' => 'Sophie',
            'email' => 'sophie.simon@hotmail.com',
        ],
        [
            'nom' => 'Leroy',
            'prenom' => 'Luc',
            'email' => 'luc.leroy@yahoo.com',
        ],
        [
            'nom' => 'Garcia',
            'prenom' => 'Anne',
            'email' => 'anne.garcia@gmail.com',
        ],
];


        // Données des entreprises
        $entreprisesData = [
        [
            'nom' => 'Tech Solutions',
            'ville' => 'Paris',
            'pays' => 'France',
        ],
        [
            'nom' => 'Global Industries',
            'ville' => 'Berlin',
            'pays' => 'Allemagne',
        ],
        [
            'nom' => 'InnoTech',
            'ville' => 'Madrid',
            'pays' => 'Espagne',
        ],
        [
            'nom' => 'EcoTech',
            'ville' => 'Rome',
            'pays' => 'Italie',
        ],
        [
            'nom' => 'NexGen Solutions',
            'ville' => 'Amsterdam',
            'pays' => 'Pays-Bas',
        ],
        [
            'nom' => 'Digital Innovators',
            'ville' => 'Lisbonne',
            'pays' => 'Portugal',
        ],
        [
            'nom' => 'EuroTech',
            'ville' => 'Bruxelles',
            'pays' => 'Belgique',
        ],
        [
            'nom' => 'Smart Systems',
            'ville' => 'Vienne',
            'pays' => 'Autriche',
        ],
        [
            'nom' => 'ScandiTech',
            'ville' => 'Stockholm',
            'pays' => 'Suède',
        ],
        [
            'nom' => 'Arctic Solutions',
            'ville' => 'Helsinki',
            'pays' => 'Finlande',
        ],
        [
            'nom' => 'Nordic Tech',
            'ville' => 'Copenhague',
            'pays' => 'Danemark',
        ],
        [
            'nom' => 'PolandTech',
            'ville' => 'Varsovie',
            'pays' => 'Pologne',
        ],
        [
            'nom' => 'Czech Innovations',
            'ville' => 'Prague',
            'pays' => 'République tchèque',
        ],
        [
            'nom' => 'Greek Tech Solutions',
            'ville' => 'Athènes',
            'pays' => 'Grèce',
        ],
        [
            'nom' => 'Hungarian Innovators',
            'ville' => 'Budapest',
            'pays' => 'Hongrie',
        ],
    ];


        $etudiants = [];
        $tuteurs = [];
        $responsables = [];
        $entreprises = [];

        foreach ($etudiantsData as $etudiantData) {
            $etudiant = new Etudiant();
            $etudiant->setNom($etudiantData['nom'])
                ->setPrenom($etudiantData['prenom'])
                ->setClasse($etudiantData['classe']);

            $manager->persist($etudiant);
            $etudiants[] = $etudiant;
        }

        foreach ($tuteursData as $tuteurData) {
            $tuteur = new Tuteur();
            $tuteur->setNom($tuteurData['nom'])
                ->setPrenom($tuteurData['prenom'])
                ->setEmail($tuteurData['email'])
                ->setTelephone($tuteurData['telephone']);

            $manager->persist($tuteur);
            $tuteurs[] = $tuteur;
        }

        

       foreach ($entreprisesData as $entrepriseData) {
            $entreprise = new Entreprise();
            $entreprise->setNom($entrepriseData['nom'])
                ->setVille($entrepriseData['ville'])
                ->setPays($entrepriseData['pays']);

            // Génération aléatoire de l'email
            $nomSansEspaces = str_replace(' ', '', strtolower($entrepriseData['nom']));
            $email = $nomSansEspaces . '@example.com';
            $entreprise->setAdresseEmail($email);

            // Génération aléatoire du site web
            $nomSansEspacesDashes = str_replace(' ', '-', strtolower($entrepriseData['nom']));
            $siteWeb = 'www.' . $nomSansEspacesDashes . '.com';
            $entreprise->setSiteInternet($siteWeb);

            $manager->persist($entreprise);
            $entreprises[] = $entreprise;
       }

        foreach ($responsablesData as $responsableData) {
            $responsable = new Responsable();
            $responsable->setNom($responsableData['nom'])
                ->setPrenom($responsableData['prenom'])
                ->setEmail($responsableData['email']);

            // Sélectionnez une entreprise aléatoire
            $entreprise = $entreprises[array_rand($entreprises)];
            $responsable->setEntreprise($entreprise);

            $manager->persist($responsable);
            $responsables[] = $responsable;
        }

        shuffle($etudiants);
        shuffle($tuteurs);
        shuffle($responsables);
        shuffle($entreprises);

        // Données des stages
        $stagesData = [
        [
            'mois' => 'Janvier',
            'annee' => '2023',
        ],
        [
            'mois' => 'Février',
            'annee' => '2023',
        ],
        [
            'mois' => 'Mars',
            'annee' => '2023',
        ],
        [
            'mois' => 'Avril',
            'annee' => '2023',
        ],
        [
            'mois' => 'Mai',
            'annee' => '2023',
        ],
        [
            'mois' => 'Juin',
            'annee' => '2023',
        ],
        [
            'mois' => 'Juillet',
            'annee' => '2023',
        ],
        [
            'mois' => 'Août',
            'annee' => '2023',
        ],
        [
            'mois' => 'Septembre',
            'annee' => '2023',
        ],
        [
            'mois' => 'Octobre',
            'annee' => '2023',
        ],
        [
            'mois' => 'Novembre',
            'annee' => '2023',
        ],
        [
            'mois' => 'Décembre',
            'annee' => '2023',
        ],
    ];


        foreach ($stagesData as $stageData) {
            $stage = new Stage();
            $stage->setEtudiant($etudiants[array_rand($etudiants)])
                ->setResponsable($responsables[array_rand($responsables)])
                ->setEntreprise($entreprises[array_rand($entreprises)])
                ->setTuteur($tuteurs[array_rand($tuteurs)])
                ->setMois($stageData['mois'])
                ->setAnnee($stageData['annee']);

            $manager->persist($stage);
        }

        foreach ($users as $userData) {
            $user = new User();
            $user->setEmail($userData['email']);
            $user->setRoles($userData['roles']);

            $hashedPassword = $this->passwordHasher->hashPassword($user, $userData['password']);
            $user->setPassword($hashedPassword);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
