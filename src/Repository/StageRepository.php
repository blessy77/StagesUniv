<?php

namespace App\Repository;

use App\Entity\Stage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Stage>
 *
 * @method Stage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stage[]    findAll()
 * @method Stage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stage::class);
    }

    public function save(Stage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Stage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Stage[] Returns an array of Stage objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Stage
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }


public function findBySearchTerm($searchTerm)
{
    $entityManager = $this->getEntityManager();

    $queryBuilder = $entityManager->createQueryBuilder()
        ->select('s')
        ->from(Stage::class, 's')
        ->leftJoin('s.responsable', 'r')
        ->leftJoin('s.etudiant', 'e')
        ->leftJoin('s.tuteur', 't')
        ->leftJoin('s.entreprise', 'en')
        ->where('s.annee LIKE :searchTerm')
        ->orWhere('s.mois LIKE :searchTerm')
        ->orWhere('CONCAT(e.prenom, \' \', e.nom) LIKE :searchTerm')
        ->orWhere('CONCAT(r.prenom, \' \', r.nom) LIKE :searchTerm')
        ->orWhere('CONCAT(t.prenom, \' \', t.nom) LIKE :searchTerm')
        ->orWhere('en.nom LIKE :searchTerm')
        ->orWhere('e.classe LIKE :searchTerm')
        ->orWhere('en.pays LIKE :searchTerm')
        ->orWhere('en.ville LIKE :searchTerm')
        ->setParameter('searchTerm', '%' . $searchTerm . '%');
    

    return $queryBuilder->getQuery()->getResult();
}


}
