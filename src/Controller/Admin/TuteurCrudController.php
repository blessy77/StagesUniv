<?php

namespace App\Controller\Admin;

use App\Entity\Tuteur;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;

class TuteurCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Tuteur::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Tuteur')
            ->setEntityLabelInPlural('Tuteurs');
            // ->setDateFormat('...')
            // ...
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(), // Champ ID masqué dans le formulaire
            TextField::new('prenom'), // Champ de texte pour le prénom
            TextField::new('nom'), // Champ de texte pour le nom
            EmailField::new('email'), // Champ d'email
            TelephoneField::new('telephone'), // Champ de numéro de téléphone
        ];
    }
}
