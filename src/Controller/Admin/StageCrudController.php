<?php

namespace App\Controller\Admin;

use App\Entity\Stage;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Stage::class;
    }

    public function configureFields(string $pageName): iterable
    {
        // Liste des mois
        $mois = [
            'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
        ];

        // Liste des années
        $annees = [];
        $anneeCourante = date('Y');
        for ($i = $anneeCourante - 10; $i <= $anneeCourante + 10; $i++) {
            $annees[$i] = $i;
        }

        return [
            IdField::new('id')->hideOnForm(), // Champ ID masqué dans le formulaire
            AssociationField::new('etudiant'), // Champ d'association avec l'entité Etudiant
            AssociationField::new('entreprise'), // Champ d'association avec l'entité Entreprise
            AssociationField::new('tuteur'), // Champ d'association avec l'entité Tuteur
            AssociationField::new('responsable'), // Champ d'association avec l'entité Responsable
            ChoiceField::new('mois') // Champ de choix pour le mois
                ->setChoices(array_combine($mois, $mois)), // Utilisation des mois comme choix
            ChoiceField::new('annee') // Champ de choix pour l'année
                ->setChoices($annees), // Utilisation des années comme choix
        ];
    }
}
