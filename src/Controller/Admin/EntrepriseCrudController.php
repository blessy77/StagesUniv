<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;

use App\Entity\Entreprise;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EntrepriseCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Entreprise::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        // Configuration générale du CRUD pour l'entité Entreprise
        return $crud
            ->setEntityLabelInSingular('Entreprise') // Étiquette pour une seule entreprise
            ->setEntityLabelInPlural('Entreprises'); // Étiquette pour plusieurs entreprises
    }

    public function configureFields(string $pageName): iterable
    {
        // Configuration des champs à afficher et à gérer dans les formulaires et les vues
        return [
            IdField::new('id') // Champ ID, masqué dans le formulaire
                ->hideOnForm(),
            TextField::new('nom'), // Champ texte pour le nom de l'entreprise
            TextField::new('ville'), // Champ texte pour la ville de l'entreprise
            TextField::new('pays'), // Champ texte pour le pays de l'entreprise
            TextField::new('siteInternet') // Champ URL pour le site internet de l'entreprise
                ->setRequired(true), // Champ requis
            EmailField::new('adresseEmail') // Champ email pour l'adresse email de l'entreprise
                ->setRequired(true), // Champ requis
        ];
    }
}
