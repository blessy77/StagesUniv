<?php

namespace App\Controller\Admin;

use App\Entity\Responsable;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ResponsableCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Responsable::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        // Configuration générale du CRUD pour l'entité Responsable
        return $crud
            ->setEntityLabelInSingular('Responsable') // Étiquette pour un seul responsable
            ->setEntityLabelInPlural('Responsables'); // Étiquette pour plusieurs responsables
    }

    public function configureFields(string $pageName): iterable
    {
        // Configuration des champs à afficher et à gérer dans les formulaires et les vues
        return [
            IdField::new('id') // Champ ID, masqué dans le formulaire
                ->hideOnForm(),
            TextField::new('prenom', 'Prénom'), // Champ texte pour le prénom du responsable, avec une étiquette personnalisée
            TextField::new('nom', 'Nom'), // Champ texte pour le nom du responsable, avec une étiquette personnalisée
            AssociationField::new('entreprise', 'Entreprise'), // Champ d'association pour l'entité Entreprise, avec une étiquette personnalisée
        ];
    }
}
