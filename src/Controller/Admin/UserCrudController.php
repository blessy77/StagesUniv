<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;

class UserCrudController extends AbstractCrudController
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(), // Champ ID affiché uniquement dans l'index
            EmailField::new('email'), // Champ d'email
            ArrayField::new('roles'), // Champ d'array pour les rôles
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $ajouterUtilisateur = Action::new('ajouterUtilisateur', 'Ajouter utilisateur')
            ->linkToRoute('app_register') // Lien vers la route "app_register"
            ->displayIf(function () {
                return true;
            })
            ->createAsGlobalAction()
            ->setIcon('fa fa-plus') // Icône pour le bouton
            ->setCssClass('btn btn-primary'); // Classe CSS pour le bouton

        return $actions
            ->add(Crud::PAGE_INDEX, $ajouterUtilisateur) // Ajout de l'action personnalisée sur la page d'index
            ->remove(Crud::PAGE_INDEX, Action::NEW); // Suppression de l'action "Nouveau" sur la page d'index
    }
}
