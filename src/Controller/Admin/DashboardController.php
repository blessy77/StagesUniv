<?php
namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Etudiant;
use App\Entity\Entreprise;
use App\Entity\Responsable;
use App\Entity\Tuteur;
use App\Entity\Stage;
use App\Entity\User;
use App\Repository\StageRepository;
use App\Repository\EtudiantRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\TuteurRepository;
use App\Repository\ResponsableRepository;
use App\Repository\UserRepository;

class DashboardController extends AbstractDashboardController
{
    private $stageRepository;
    private $etudiantRepository;
    private $entrepriseRepository;
    private $tuteurRepository;
    private $responsableRepository;
    private $userRepository;

    public function __construct(
        StageRepository $stageRepository,
        EtudiantRepository $etudiantRepository,
        EntrepriseRepository $entrepriseRepository,
        TuteurRepository $tuteurRepository,
        ResponsableRepository $responsableRepository,
        UserRepository $userRepository
    ) {
        $this->stageRepository = $stageRepository;
        $this->etudiantRepository = $etudiantRepository;
        $this->entrepriseRepository = $entrepriseRepository;
        $this->tuteurRepository = $tuteurRepository;
        $this->responsableRepository = $responsableRepository;
        $this->userRepository = $userRepository;
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // Compter le nombre total de stages, étudiants, entreprises, tuteurs, responsables et utilisateurs
        $nbStages = $this->stageRepository->count([]);
        $nbEtudiants = $this->etudiantRepository->count([]);
        $nbEntreprises = $this->entrepriseRepository->count([]);
        $nbTuteurs = $this->tuteurRepository->count([]);
        $nbResponsables = $this->responsableRepository->count([]);
        $nbUsers = $this->userRepository->count([]);

        // Rendre la vue du tableau de bord avec les données comptées
        return $this->render('admin/dashboard.html.twig', [
            'nbStages' => $nbStages,
            'nbEtudiants' => $nbEtudiants,
            'nbEntreprises' => $nbEntreprises,
            'nbTuteurs' => $nbTuteurs,
            'nbResponsables' => $nbResponsables,
            'nbUsers' => $nbUsers
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        // Configuration générale du tableau de bord
        return Dashboard::new()
            ->setTitle('Stages - Administration')
            ->renderContentMaximized();
    }

    public function configureMenuItems(): iterable
    {
        // Configuration des éléments du menu de navigation
        yield MenuItem::linkToRoute('Retour à l\'accueil', 'fa fa-home', 'app_home');
        yield MenuItem::linkToCrud('Etudiant', 'fa-solid fa-user-graduate', Etudiant::class);
        yield MenuItem::linkToCrud('Entreprise', 'fa-solid fa-building', Entreprise::class);
        yield MenuItem::linkToCrud('Responsable', 'fa-solid fa-user-tie', Responsable::class);
        yield MenuItem::linkToCrud('Tuteur', 'fa-solid fa-user-tie', Tuteur::class);
        yield MenuItem::linkToCrud('Stage', 'fa-solid fa-graduation-cap', Stage::class);
        yield MenuItem::linkToCrud('User', 'fa-sharp fa-solid fa-user-plus', User::class);
    }
}
