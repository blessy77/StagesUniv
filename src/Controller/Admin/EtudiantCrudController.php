<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;

use App\Entity\Etudiant;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EtudiantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Etudiant::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        // Configuration générale du CRUD pour l'entité Etudiant
        return $crud
            ->setEntityLabelInSingular('Etudiant') // Étiquette pour un seul étudiant
            ->setEntityLabelInPlural('Etudiants'); // Étiquette pour plusieurs étudiants
            // ->setDateFormat('...') // Format de date personnalisé
            // ...
    }

    public function configureFields(string $pageName): iterable
    {
        // Configuration des champs à afficher et à gérer dans les formulaires et les vues
        return [
            IdField::new('id') // Champ ID, masqué dans le formulaire
                ->hideOnForm(),
            TextField::new('nom'), // Champ texte pour le nom de l'étudiant
            TextField::new('prenom'), // Champ texte pour le prénom de l'étudiant
            TextField::new('classe'), // Champ texte pour la classe de l'étudiant
        ];
    }
}
