<?php

namespace App\Controller;

use App\Repository\StageRepository;
use App\Repository\EtudiantRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\TuteurRepository;
use App\Repository\ResponsableRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(
        StageRepository $stageRepository,
        EtudiantRepository $etudiantRepository,
        EntrepriseRepository $entrepriseRepository,
        TuteurRepository $tuteurRepository,
        ResponsableRepository $responsableRepository
    ): Response {
        $nbStages = $stageRepository->count([]);
        $nbEtudiants = $etudiantRepository->count([]);
        $nbEntreprises = $entrepriseRepository->count([]);
        $nbTuteurs = $tuteurRepository->count([]);
        $nbResponsables = $responsableRepository->count([]);
        
        return $this->render('home/index.html.twig', [
            'nbStages' => $nbStages,
            'nbEtudiants' => $nbEtudiants,
            'nbEntreprises' => $nbEntreprises,
            'nbTuteurs' => $nbTuteurs,
            'nbResponsables' => $nbResponsables,
        ]);
    }
}
