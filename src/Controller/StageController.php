<?php

namespace App\Controller;

use App\Entity\Stage;
use App\Repository\StageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/stage')]
class StageController extends AbstractController
{
    #[Route('/', name: 'app_stage_index', methods: ['GET'])]
    public function index(Request $request, StageRepository $stageRepository): Response
    {
        $searchTerm = $request->query->get('search');
        $stages = [];

        if ($searchTerm) {
            $stages = $stageRepository->findBySearchTerm($searchTerm);
        } else {
            $stages = $stageRepository->findAll();
        }

        return $this->render('stage/index.html.twig', [
            'stages' => $stages,
            'searchTerm' => $searchTerm,
        ]);
    }

    #[Route('/{id}', name: 'app_stage_show', methods: ['GET'])]
    public function show(Stage $stage): Response
    {
        return $this->render('stage/show.html.twig', [
            'stage' => $stage,
        ]);
    }
}
