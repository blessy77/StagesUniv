<?php

namespace App\Controller;

use App\Entity\Tuteur;
use App\Repository\TuteurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/tuteur')]
class TuteurController extends AbstractController
{
    #[Route('/', name: 'app_tuteur_index', methods: ['GET'])]
    public function index(TuteurRepository $tuteurRepository): Response
    {
        return $this->render('tuteur/index.html.twig', [
            'tuteurs' => $tuteurRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'app_tuteur_show', methods: ['GET'])]
    public function show(Tuteur $tuteur): Response
    {
        return $this->render('tuteur/show.html.twig', [
            'tuteur' => $tuteur,
        ]);
    }
}
