# Projet Stage

Ce projet est une application de gestion de stages pour les étudiants d'une école. Il permet de gérer les informations liées aux étudiants, aux entreprises d'accueil, aux tuteurs et aux responsables de stages.

## Entités

### Stage

- `id` (int) : Identifiant unique du stage
- `etudiant` (Etudiant) : Référence à l'étudiant associé au stage
- `mois` (string) : Mois de début du stage
- `annee` (string) : Année de début du stage

### Etudiant

- `id` (int) : Identifiant unique de l'étudiant
- `nom` (string) : Nom de l'étudiant
- `prenom` (string) : Prénom de l'étudiant
- `classe` (string) : Classe de l'étudiant

### Entreprise

- `id` (int) : Identifiant unique de l'entreprise
- `nom` (string) : Nom de l'entreprise
- `ville` (string) : Ville de l'entreprise
- `pays` (string) : Pays de l'entreprise

### Tuteur

- `id` (int) : Identifiant unique du tuteur
- `nom` (string) : Nom du tuteur
- `prenom` (string) : Prénom du tuteur
- `entreprise` (Entreprise) : Référence à l'entreprise associée au tuteur

### Responsable

- `id` (int) : Identifiant unique du responsable
- `nom` (string) : Nom du responsable
- `prenom` (string) : Prénom du responsable
- `entreprise` (Entreprise) : Référence à l'entreprise associée au responsable

## Développement

Le projet Stage est développé en utilisant les technologies suivantes :

- Langage de programmation : PHP
- Framework : Symfony
- Base de données : MySQL

### Prérequis

Avant de commencer le développement, assurez-vous d'avoir les éléments suivants installés sur votre machine :

- PHP (version 7.4 ou supérieure)
- Composer (pour gérer les dépendances PHP)
- MySQL (ou tout autre système de gestion de base de données supporté par Symfony)

### Installation

1. Clonez ce dépôt de code sur votre machine :

   ```shell
   git clone <URL_DU_REPO>
   ```

2. Accédez au répertoire du projet :
   ```shell
   cd projet-stage
   ```
3. Configurez l'URL de la base de données dans le fichier .env en remplaçant DATABASE_URL par les informations appropriées :
   ```shell
   DATABASE_URL=mysql://user:password@host:port/database_name
   ```
   Remplacez user, password, host, port et database_name par les valeurs spécifiques à votre configuration de base de données.
4. Créez la base de données en exécutant la commande suivante :
   ```shell
   php bin/console doctrine:database:create
   ```
5. Effectuez les migrations en exécutant la commande suivante :
   ```shell
   php bin/console doctrine:migrations:migrate
   ```
6. Chargez les fixtures en exécutant la commande suivante :
   ```shell
   php bin/console doctrine:fixtures:load
   ```
7. Démarrez le serveur de développement :

   ```shell
   symfony server:start
   ```

8. Accédez à l'application dans votre navigateur à l'adresse suivante :
   ```shell
   http://localhost:8000
   ```
